﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace props
{
    class Tv
    {
        protected short volume;

        public short Volume {
            get
            {
                return this.volume;
            }
            set
            {
                if (value > 100)
                    this.volume = 100;
                else if (value < 0)
                    this.volume = 0;
                else
                    this.volume = value;
            }
        }
    }

    class GeometricFigure // Можно было его сделать абстрактным, но это будет в 4 задании.
    {
        protected double x;
        protected double y;

        public GeometricFigure(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
        
        public double X 
        {
            get 
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public double Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }

        public virtual void Draw() // Возможно, нужно было просто определить два метода в наследниках, но я в этом не уверен.
        {
            Console.WriteLine("Геометрическая фигура");
        }
    }

    class Triangle : GeometricFigure
    {

        public Triangle(double x, double y) : base (x,y) { }

        protected double area;

        public double Area
        {
            get
            {
                return area;
            }
            set
            {
                area = value;
            }
        }

        public override void Draw()
        {
            Console.WriteLine("Треугольник");
        }
    }
    class Square : GeometricFigure
    {
        public Square(double x, double y) : base(x, y) { }
       

        protected double sideSize;

        public double SideSize
        {
            get
            {
                return sideSize;
            }
            set
            {
                sideSize = value;
            }
        }

        public override void Draw()
        {
            Console.WriteLine("Квадрат");
        }
    }

    class Warrior
    {
        public Warrior(double health)
        {
            this.health = health;
        }

        protected double health;

        public double Health
        {
            get
            {
                return this.health;
            }
            set
            {
                if (value <= 0)
                {
                    Console.WriteLine("Убит!");
                    this.health = 0;
                }
                else
                {
                    this.health = value;
                }
            }
        }

        public virtual void GetDamage(double damage)
        {
            this.Health -= damage * 0.9;
            Console.WriteLine($"Урон: {damage}, Отнятое здоровье: {damage * 0.9}");
        }
    }

    class WarriorLightArmor : Warrior
    {
        public WarriorLightArmor(double health) : base(health)
        {
            this.Health += 20;
        }

        public override void GetDamage(double damage)
        {
            this.Health -= damage * 0.7;
            Console.WriteLine($"Урон: {damage}, Отнятое здоровье: {damage * 0.7}");
        }
    }

    class WarriorHeavyArmor : Warrior
    {
        public WarriorHeavyArmor(double health) : base(health)
        {
            this.Health += 40;
        }

        public override void GetDamage(double damage)
        {
            this.Health -= damage * 0.3;
            Console.WriteLine($"Урон: {damage}, Отнятое здоровье: {damage * 0.3}");
        }
    }

    abstract class Human
    {

        public string Name { get; set; }
        abstract public void SayHello();
    }

    class Russian : Human
    {
        public override void SayHello()
        {
            Console.WriteLine("Привет");
        }

    }
    class Ukrainian : Human
    {
        public override void SayHello()
        {
            Console.WriteLine("Привiт");
        }

    }
    class American : Human
    {
        public override void SayHello()
        {
            Console.WriteLine("Hi");
        }

    }


    class Program
    {
        static void Main(string[] args)
        {
            //region 1 задача
            Tv tv = new Tv();

            tv.Volume = 12;
            Console.WriteLine(tv.Volume);

            tv.Volume = 101;
            Console.WriteLine(tv.Volume);

            tv.Volume = -1;
            Console.WriteLine(tv.Volume);
            //endregion

            //region 2 задача

            Triangle triangle = new Triangle(10,20.5);
            triangle.Draw();
            Square square = new Square(10,20.5);
            square.Draw();
            //endregion

            //region 3 задача

            WarriorLightArmor warriorLightArmor = new WarriorLightArmor(10);

            warriorLightArmor.GetDamage(50);

            WarriorHeavyArmor warriorHeavyArmor = new WarriorHeavyArmor(10);

            warriorHeavyArmor.GetDamage(50);
            //endregion

            //region 4 задача

            Russian russian = new Russian();
            russian.SayHello();

            Ukrainian ukrainian = new Ukrainian();
            ukrainian.SayHello();

            American american = new American();
            american.SayHello();

            //endregion
        }
    }
}
